#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <string.h>

#define INPUT_SIZE 54

#define RED_LED 5
#define GREEN_LED 6
#define BLUE_LED 7

LiquidCrystal_I2C lcd(0x27, 16, 2);

int incomingByte = 0;

void setup()
{
  Serial.begin(9600);

  lcd.init();
  lcd.backlight();
}

void loop()
{
  if (Serial.available() > 0)
  {
    char input[INPUT_SIZE + 1];
    byte size = Serial.readBytes(input, INPUT_SIZE);
    input[size] = 0;

    lcd.clear();

    // Split input string to get commands
    char *data = strtok(input, "£");

    while (data != 0)
    {
      String value(data);
      String command = value.substring(0, 3);
      value.remove(0, 3);
      value.trim();

      if (command == "TX1")
      {
        lcdLine(0, value);
      }

      if (command == "TX2")
      {
        lcdLine(1, value);
      }

      if (command == "COL")
      {
        rgbLed(value);
      }

      data = strtok(0, "£");
    }
  }  
}

/**
 * Print line to LCD
 */
void lcdLine(int line, String value)
{
  lcd.setCursor(0, line);
  lcd.print(value);
}

/**
 * Convert HEX to RGB
 * Set RGB LED values
 */
void rgbLed(String hex)
{
  if (hex.length() != 6)
  {
    return;
  }

  // Convert HEX to RGB value
  long number = (long)strtol(&hex[0], NULL, 16);
  int red = number >> 16;
  int green = number >> 8 & 0xFF;
  int blue = number & 0xFF;

  analogWrite(RED_LED, red);
  analogWrite(GREEN_LED, green);
  analogWrite(BLUE_LED, blue);
}