/* eslint-disable no-console */
import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import router from './routes';
import SerialPort from 'serialport';
import arg from 'arg';

const args = arg({ '--port': String });

const app = express();

const port = process.env.PORT || 3000;
const arduinoPort = args['--port'];

if (!arduinoPort) {
    console.warn('\x1b[31mPlease provide Serial Port with --port flag');
    process.exit(1);
}

let serialPort = null;

serialPort = new SerialPort(
    arduinoPort,
    {
        baudRate: 9600,
    },
    (err) => {
        if (err) {
            console.warn('\x1b[31mCan\'t connect to Arduino port');
            process.exit(1);
        }
    }
);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use('/', router(serialPort));

app.listen(port);

export default app;
