import SerialPort from 'serialport';
import { Router } from 'express';
import taskRoutes from './taskRoutes';

const defaultRouter = (serialPort: SerialPort): Router => {
    const router = Router();

    router.use('/tasks', taskRoutes(serialPort));

    return router;
};

export default defaultRouter;
