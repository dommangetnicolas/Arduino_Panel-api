import SerialPort from 'serialport';
import { Router } from 'express';
import createTask from '../controllers/tasks/createTask';

const taskRoutes = (serialPort: SerialPort): Router => {
    const router = Router();

    router.route('/')
        .post((req, res) => createTask(req, res, serialPort));

    return router;
};

export default taskRoutes;
