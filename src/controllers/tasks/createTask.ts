import SerialPort from 'serialport';
import { Request, Response } from 'express';

const createTask = (
    req: Request,
    res: Response,
    serialPort: SerialPort
): void => {
    const { line1, line2, color } = req.body;

    if (line1) {
        serialPort.write(`£TX1${line1}`);
    }

    if (line2) {
        serialPort.write(`£TX2${line2}`);
    }

    if (color) {
        serialPort.write(`£COL${color.replace('#', '')}`);
    }

    return res.status(200).end();
};

export default createTask;
